import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  // title = 'my-app';
  // data = [
  //   {name : "John Doe", DOB : "27th January, 1970", address : "27, Yemen City"},
  //   {name : "John keen", DOB : "21th May, 1981", address : "3, Edgar Buildings"},
  //   {name : "Nick Parry", DOB : "15th December, 1998", address : "61, Wellfield Road"}
  // ];
  // updateIndex=-1;

  // onDelete(item) {
  //   let dIndex = -1;
  //   this.data.forEach((ditem,i) => {
  //     if(ditem.name===item.name) {
  //       dIndex=i;
  //     }
  //   });
  //   if(dIndex !== -1) {
  //     this.data.splice(dIndex,1);
  //   }
  // }

  // onUpdate(index) {
  //   if(this.updateIndex===-1) {
  //     this.updateIndex=index;
  //   } else {
  //     this.updateIndex=-1;
  //   }
    
  // }

  services: any = [
    {
      type: 'Basic',
      amount: 2600.0,
      duration: 'Month',
      discount: 10,
      features: [
        'Approximate 500 Employees',
        'Small to Medium Size Enterprises',
        'Clients can only access the Starter Level',
        'Admin can assign Basic and Intermediate to specific users',
        'Clients can access both the Intermediate and Starter Levels',
        'Clients can access all three levels; e.g Starter, Intermediate, and Expert'
      ]
    },
    {
      type: 'Standard',
      amount: 750.0,
      duration: 'Month',
      discount: 10,
      features: [
        'Approximate 500 Employees',
        'Small to Medium Size Enterprises',
        'Clients can only access the Starter Level',
        'Admin can assign Basic and Intermediate to specific users',
        'Clients can access all three levels; e.g Starter, Intermediate, and Expert'
      ]
    },
    {
      type: 'Advanced',
      amount: 2500,
      duration: 'Month',
      discount: 10,
      features: [
        'Approximate 500 Employees',
        'Small to Medium Size Enterprises',
        'Clients can only access the Starter Level',
        'Admin can assign Basic and Intermediate to specific users'
      ]
    }
  ];

}
